#include <iostream>
#include <thread>
#include <chrono>
#include <pigpio.h>
#include "opencv2/opencv.hpp"

int main ()
{
    if (gpioInitialise() < 0)
    {
       std::cout << "pigpio initialisation failed."  << std::endl;
       return -1;
    }
    std::cout << "pigpio initialised okay."  << std::endl;

    for (int i = 0; i < 28; i++)
    {
        gpioSetMode(i, PI_OUTPUT);
    }

    bool bVal = false;
    cv::VideoCapture capture(0);
    cv::Mat frame;
    while(true)
    {
        capture >> frame;
        if(!frame.empty())
        {
            cv::imshow("frame", frame);
        }

        for (int i = 0; i < 28; i++)
        {
            gpioWrite(i, bVal);
        }
        bVal = !bVal;
        cv::waitKey(30);
    }
    
    gpioTerminate();
    return 0;
}
