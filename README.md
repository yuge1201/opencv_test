# opencv_test

#### 介绍
RaspberryPi 4B/CM4 测试程序

#### 依赖
* pigpio:

  ```
  sudo apt install pigpio
  ```

* OpenCV

   安装教程：https://qengineering.eu/install-opencv-4.5-on-raspberry-64-os.html 

#### 使用说明

```
git clone https://gitee.com/yuge1201/opencv_test.git
cd opencv_test
mkdir build
cd build
cmake ..
make
sudo bin/test
```

#### 效果

每秒钟翻转树莓派全部的IO脚(1-28)约15次，并打开摄像头拍摄并显示.

